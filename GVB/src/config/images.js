const LOGO = require('../assets/logo.png');
const LOGO_WITHOUT_SLOGON = require('../assets/logo2.png');

export {LOGO, LOGO_WITHOUT_SLOGON};
