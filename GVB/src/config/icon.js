const ACCOUNT = require('../assets/acc_icon.png');
const SHOW_PASS = require('../assets/show_password.png');
const HIDE_PASS = require('../assets/hide_password.png');
const LOCK = require('../assets/lock_icon.png');

export {ACCOUNT, SHOW_PASS, HIDE_PASS, LOCK};
