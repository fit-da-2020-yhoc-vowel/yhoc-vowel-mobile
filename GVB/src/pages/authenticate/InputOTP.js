import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  Button,
  TouchableHighlight,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Input from '../../components/Input';
import {ACCOUNT} from '../../config/icon';

export default function InputOTP(props) {
  const [data, setData] = useState({
    number: {
      value: '',
      isValid: true,
    },
    text: {
      value: '',
      isValid: true,
    },
    password: {
      value: '',
      isValid: true,
    },
  });

  // componentDidMount() {
  //   this.runTimmer();
  // }

  // componentDidUpdate() {
  //   const {timmer} = this.state;
  //   if (timmer === 0) {
  //     this.clearTimmer();
  //   }
  //   if (timmer === 60) {
  //     this.runTimmer();
  //   }
  // }

  // componentWillUnmount() {
  //   if (this.interval) {
  //     this.clearTimmer();
  //   }
  // }

  // runTimmer = () => {
  //   this.interval = setInterval(() => {
  //     this.setState(prevState => ({timmer: prevState.timmer - 1}));
  //   }, 1000);
  // };

  // clearTimmer = () => {
  //   clearInterval(this.interval);
  // };

  // renderTimmer = () => {
  //   const {timmer} = this.state;
  //   let minutes = parseInt(timmer / 60, 10);
  //   let seconds = timmer % 60;
  //   return (
  //     <View style={{alignItems: 'center', justifyContent: 'center'}}>
  //       <Text style={styles.heading}>{`${minutes} : ${seconds}`}</Text>
  //     </View>
  //   );
  // };

  const phoneNumber = props.phoneNumber || '+84123456789';
  return (
    <View>
      <LinearGradient
        colors={['#2A6ADA', '#4EAFE9']}
        style={styles.mainWrapper}>
        <TouchableHighlight
          underlayColor="transparent"
          style={styles.backBtn}
          onPress={() => console.log('pressed')}>
          <Image
            style={styles.backIcon}
            source={require('../../assets/back_icon.png')}
          />
        </TouchableHighlight>
        <View style={styles.contentWrapper}>
          <Image
            style={styles.logo}
            source={require('../../assets/logo2.png')}
          />
          <Text style={styles.desc}>
            Nhập mã xác thực đã được gửi về số điện thoại
          </Text>
          <Text style={styles.heading}>{phoneNumber}</Text>
          {/* <View style={styles.otpInput}>
              <TextInput
                autoFocus
                style={styles.input}
                keyboardType="numeric"
                placeholder="-"
                maxLength={1}
              />
              <TextInput
                style={styles.input}
                keyboardType="numeric"
                placeholder="-"
                maxLength={1}
              />
              <TextInput
                style={styles.input}
                keyboardType="numeric"
                placeholder="-"
                maxLength={1}
              />
              <TextInput
                style={styles.input}
                keyboardType="numeric"
                placeholder="-"
                maxLength={1}
              />
            </View> */}
          <Input
          password={true}
            icon={ACCOUNT}
            data={data.text}
            title="demo"
            name="text"
            label="Demo input"
            placeholder="Demo"
            handleBlur={e => console.log(e.text)}
            handleChange={input => {
              setData(prevState => ({
                ...prevState,
                text: {...prevState.text, value: input},
              }));
            }}
          />
          <Input
            data={data.number}
            keyboardType="numeric"
            name="number"
            title="demo number"
            label="Demo number"
            placeholder="Number"
            handleBlur={e => console.log(e.text)}
            handleChange={input => {
              setData(prevState => ({
                ...prevState,
                number: {...prevState.number, value: input},
              }));
            }}
          />
          <Input
            data={data.password}
            name="password"
            title="demo password"
            label="Demo password"
            placeholder="Password"
            handleBlur={e => console.log(e.text)}
            handleChange={input => {
              setData(prevState => ({
                ...prevState,
                password: {...prevState.password, value: input},
              }));
            }}
          />
          {/* {this.renderTimmer()} */}
          <TouchableHighlight
            style={styles.button}
            underlayColor="#4EAFE9"
            onPress={() => this.setState({timmer: 60})}>
            <Text style={styles.buttonText}>{'Gửi lại mã'.toUpperCase()}</Text>
          </TouchableHighlight>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            resizeMode="stretch"
            style={{flexShrink: 1}}
            source={require('../../assets/footer_decor.png')}
          />
        </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  mainWrapper: {
    display: 'flex',
    position: 'relative',
    height: '100%',
  },
  logo: {
    width: 90,
    height: 90,
    resizeMode: 'contain',
  },
  footer: {
    width: '100%',
    height: 'auto',
    zIndex: 0,
  },
  heading: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 20,
    marginBottom: 16,
    fontFamily: 'Roboto',
  },
  desc: {
    marginTop: 32,
    color: 'white',
    textAlign: 'center',
    marginBottom: 8,
    fontFamily: 'Roboto',
  },
  input: {
    width: 50,
    height: 80,
    backgroundColor: 'white',
    borderRadius: 18,
    padding: 8,
    textAlign: 'center',
    margin: 8,
    fontSize: 30,
    fontWeight: 'bold',
  },
  button: {
    width: '100%',
    backgroundColor: '#2B6CDB',
    height: 40,
    borderRadius: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    marginBottom: 8,
    zIndex: 99, // works on ios
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    alignItems: 'center',
    fontFamily: 'Roboto',
  },
  contentWrapper: {
    flex: 1,
    padding: 32,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
  otpInput: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  backBtn: {
    alignSelf: 'flex-start',
    width: 40,
    height: 40,
    marginLeft: 16,
    marginTop: 16,
  },
  backIcon: {
    resizeMode: 'contain',
  },
});
