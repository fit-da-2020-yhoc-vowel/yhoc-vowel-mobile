import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  Button,
  TouchableHighlight,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default function InputPhoneNumber() {
  return (
    <View>
      <LinearGradient
        colors={['#2A6ADA', '#4EAFE9']}
        style={styles.mainWrapper}>
        <View style={styles.contentWrapper}>
          <Image
            style={styles.logo}
            source={require('../../assets/logo2.png')}
          />
          <Text style={styles.heading}>Nhập số điện thoại</Text>
          <Text style={styles.desc}>
            Dùng số điện thoại để đăng ký hoặc đăng nhập vào ứng dụng
          </Text>
          <TextInput
            style={styles.input}
            keyboardType="numeric"
            placeholder="Nhập số điện thoại"
          />
          <TouchableHighlight
            style={styles.button}
            underlayColor="#4EAFE9"
            onPress={() => console.log('pressed')}>
            <Text style={styles.buttonText}>{'Tiếp tục'.toUpperCase()}</Text>
          </TouchableHighlight>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            resizeMode="stretch"
            style={{flexShrink: 1}}
            source={require('../../assets/footer_decor.png')}
          />
        </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  mainWrapper: {
    display: 'flex',
    position: 'relative',
    height: '100%',
  },
  logo: {
    width: 90,
    height: 90,
    resizeMode: 'contain',
  },
  footer: {
    width: '100%',
    height: 'auto',
  },
  heading: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 20,
    marginTop: 32,
    marginBottom: 8,
  },
  desc: {
    color: 'white',
    textAlign: 'center',
    marginBottom: 16,
  },
  input: {
    width: '100%',
    height: 40,
    backgroundColor: 'white',
    borderRadius: 18,
    padding: 8,
    textAlign: 'center',
    marginTop: 8,
    marginBottom: 8,
  },
  button: {
    width: '100%',
    backgroundColor: '#2B6CDB',
    height: 40,
    borderRadius: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    marginBottom: 8,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    alignItems: 'center',
  },
  contentWrapper: {
    flex: 1,
    padding: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
