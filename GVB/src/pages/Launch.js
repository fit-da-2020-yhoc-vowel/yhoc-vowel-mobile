import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {LOGO} from '../config/images';

export default function LaunchScreen() {
  return (
    <View>
      <LinearGradient
        colors={['#2A6ADA', '#4EAFE9']}
        style={styles.mainWrapper}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Image style={styles.imgFluid} source={LOGO} />
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            resizeMode="stretch"
            style={{flexShrink: 1}}
            source={require('../assets/footer_decor.png')}
          />
        </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  mainWrapper: {
    display: 'flex',
    position: 'relative',
    height: '100%',
  },
  imgFluid: {
    width: '80%',
    height: 200,
    resizeMode: 'contain',
  },
  footer: {
    width: '100%',
    height: 'auto',
  },
});
