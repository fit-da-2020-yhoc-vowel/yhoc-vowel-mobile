import React from 'react';
import {StyleSheet} from 'react-native';
import {color1} from '../config/color';

const inputStyle = StyleSheet.create({
  wrapper: {
    width: '100%',
    position: 'relative',
    marginTop: 8,
    marginBottom: 8,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 16,
    fontFamily: 'Roboto',
  },
  label: {
    position: 'absolute',
    top: 2,
    left: 0,
    fontSize: 12,
    paddingLeft: 8,
    color: color1,
    fontFamily: 'Roboto',
  },
  wrapperInput: {
    flex: 1,
  },
  input: {
    width: '100%',
    height: 54,
    fontSize: 16,
    borderRadius: 16,
    padding: 8,
    textAlign: 'left',
    fontFamily: 'Roboto',
  },
  highLight: {
    borderColor: 'red',
    borderWidth: 1,
  },
  errMessage: {
    fontSize: 12,
    color: 'red',
    textAlign: 'right',
    paddingTop: 2,
    fontStyle: 'italic',
    fontFamily: 'Roboto',
  },
  icon: {
    paddingLeft: 8,
  },
  rightIcon: {
    paddingRight: 8,
  }
});

export default inputStyle;
