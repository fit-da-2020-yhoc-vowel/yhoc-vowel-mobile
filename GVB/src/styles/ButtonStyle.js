import {StyleSheet} from 'react-native';

const buttonStyle = StyleSheet.create({
  button: {
    width: '100%',
    backgroundColor: '#2B6CDB',
    height: 40,
    borderRadius: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    marginBottom: 8,
    zIndex: 99, // works on ios
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    alignItems: 'center',
    fontFamily: 'Roboto',
  },
});
export default buttonStyle;
