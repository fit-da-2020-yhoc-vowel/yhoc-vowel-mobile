import React from 'react';
import {StyleSheet} from 'react-native';
import {color1} from '../config/color';

const BottomNav = StyleSheet.create({
  practiceButton: {
    padding: 8,
    backgroundColor: '#00BBD3',
    borderRadius: 50,
    marginTop: -30,
  },
});

export default BottomNav;
