import React, {useState, useEffect, useRef} from 'react';
import {TextInput, View, Text, Image, TouchableOpacity} from 'react-native';
import inputStyle from '../styles/InputStyle';
import {SHOW_PASS, HIDE_PASS} from '../config/icon';

const Input = props => {
  const [showLable, setShowLabel] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const inputEl = useRef(null);
  const {
    label,
    type,
    keyboardType,
    placeholder,
    name,
    data,
    icon,
    password,
  } = props;

  useEffect(() => {
    setShowLabel(data.value.length > 0 ? true : false);
  }, [data.value]);

  const handleFocus = () => {
    setShowLabel(true);
  };
  const handleBlur = () => {
    if (data.value.length === 0) {
      setShowLabel(false);
    }
  };
  const handleShowPassword = () => {
    setShowPassword(true);
  };
  const handleHidePassword = () => {
    setShowPassword(false);
  };
  const {value, errMessage, isValid} = data;
  return (
    <View style={inputStyle.wrapper}>
      <View style={inputStyle.icon}>{icon && <Image source={icon} />}</View>
      <View style={inputStyle.wrapperInput}>
        <TextInput
          ref={inputEl}
          style={{...inputStyle.input, ...(!isValid && inputStyle.highLight)}}
          type={type}
          name={name}
          keyboardType={keyboardType}
          placeholder={placeholder}
          value={value}
          secureTextEntry={showPassword}
          onFocus={handleFocus}
          onChangeText={input => {
            props.handleChange(input);
          }}
          onBlur={handleBlur}
        />
        {showLable && <Text style={inputStyle.label}>{label}</Text>}
        {!isValid && <Text style={inputStyle.errMessage}>{errMessage}</Text>}
      </View>
      {password && (
        <View style={inputStyle.rightIcon}>
          {!showPassword ? (
            <TouchableOpacity onPress={handleShowPassword}>
              <Image source={HIDE_PASS} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={handleHidePassword}>
              <Image source={SHOW_PASS} />
            </TouchableOpacity>
          )}
        </View>
      )}
    </View>
  );
};

export default Input;
