import React from 'react';
import {Image, Text, View} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Launch from '../pages/Launch';
import InputOTP from '../pages/authenticate/InputOTP';
import InputPhoneNumber from '../pages/authenticate/InputPhoneNumber';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {bottomNav} from '../styles/';

const BottomTabs = createBottomTabNavigator(
  {
    history: Launch,
    doctor: Launch,
    practice: Launch,
    progress: Launch,
    profile: Launch,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        if (routeName === 'practice') {
          return (
            <View style={bottomNav.practiceButton}>
              <Icon name="keyboard-voice" size={60} color="white" />
            </View>
          );
        } else {
          return routeName === 'doctor' ? (
            <MaterialCommunityIcons name="doctor" size={30} color={tintColor} />
          ) : (
            <Icon name={getIcon(routeName)} size={30} color={tintColor} />
          );
        }
      },
      tabBarLabel: ({tintColor}) => {
        const {routeName} = navigation.state;
        return routeName === 'practice' ? null : (
          <Text
            style={{color: `${tintColor}`, textAlign: 'center', fontSize: 12}}>
            {getLable(routeName)}
          </Text>
        );
      },
    }),
    tabBarOptions: {
      activeTintColor: '#00BBD3',
      inactiveTintColor: '#BDBDBD',
      initialRouteName: 'Dashboard',
      style: {
        height: 64,
        paddingBottom: 4,
      },
    },
    initialRouteName: 'practice',
  },
);

const getLable = name => {
  switch (name) {
    case 'practice':
      return 'Luyện tập';
    case 'history':
      return 'Lịch sử';
    case 'doctor':
      return 'Bác sĩ';
    case 'progress':
      return 'Tiến độ';
    case 'profile':
      return 'Hồ sơ';
    default:
      return '';
  }
};
const getIcon = name => {
  switch (name) {
    case 'practice':
      return 'keyboard-voice';
    case 'history':
      return 'history';
    case 'doctor':
      return 'local-hospital';
    case 'progress':
      return 'insert-chart';
    case 'profile':
      return 'person';
    default:
      return '';
  }
};

export default createAppContainer(BottomTabs);
