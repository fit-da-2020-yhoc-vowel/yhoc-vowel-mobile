import React from 'react';
import {Text, TouchableHighlight} from 'react-native';
import {buttonStyle} from '../styles/';

const Button = props => {
  const {text, handlePress} = props;
  return (
    <TouchableHighlight
      style={buttonStyle.button}
      underlayColor="#4EAFE9"
      onPress={handlePress}>
      <Text style={buttonStyle.buttonText}>{text.toUpperCase()}</Text>
    </TouchableHighlight>
  );
};

export default Button;
